package piglatintranslator;

public class Translator {
	
	public static final String NIL = "nil";
	private static String phrase;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getInputPhrase() {
		return phrase;
	}

	public String translate() {
		if(phrase.isBlank()) {
			return NIL;
		}else if(findPunctuationMarks()) {
			return translatePhraseWithMoreWordsAndPunctuationMarks();	
	     }else if(findMinusSign() != -1) {
			return translatePhraseWithMoreWordsAndMinusSign();
		}else if(findSpace() != -1) {
			return translatePhraseWithMoreWordsAndSpace();
		}else if(startWithVowel()) {
			if(phrase.endsWith("y")) {
				return phrase + "nay";
			}else if(endWithVowel()) {
				return phrase + "yay";
			}else if(!endWithVowel()) {
				return phrase + "ay";
			}
		}else if(!startWithVowel()) {
			char first_letter = phrase.charAt(0);
			char second_letter = phrase.charAt(1);
			String newPhrase = phrase.substring(1);
			if(isAVowel(second_letter)) {
				return newPhrase + first_letter + "ay";
			}else if(!isAVowel(second_letter)) {
				String new_Phrase = phrase.substring(2);
				return new_Phrase + first_letter + second_letter + "ay";
			}	
		}
		return null;
		
	}
	
	private boolean startWithVowel() {
		return (phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u"));
	}
	
	private boolean endWithVowel() {
		return (phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	private static boolean isAVowel(char lettera) {
		return (lettera == 'a' || lettera == 'e' || lettera == 'i' || lettera == 'o' || lettera == 'u');
	}
	
    private static int findMinusSign(){
		int result = -1;
		if(phrase.indexOf("-") != -1) {
			result = phrase.indexOf("-");
		}
		return result;
	}
	
	private static int findSpace() {
		int result = -1;
		if(phrase.indexOf(" ") != -1) {
			result = phrase.indexOf(" ");
		}
		return result;
	}

	private String translatePhraseWithMoreWordsAndMinusSign() {
		if(findMinusSign() != -1) {
			int divider = findMinusSign();
			int string_length = phrase.length();
			String first_phrase = phrase.substring(0, divider);
			String second_phrase = phrase.substring(divider+1, string_length);
			Translator translator_1 = new Translator(first_phrase);
			String first_piece = translator_1.translate();
			Translator translator_2 = new Translator(second_phrase);
			String second_piece = translator_2.translate();
		    return first_piece + "-" + second_piece;
		}
		return null;
	}
	
	private String translatePhraseWithMoreWordsAndSpace() {
		if(findSpace() != -1) {
			int divider = findSpace();
			int string_length = phrase.length();
			String first_phrase = phrase.substring(0, divider);
			String second_phrase = phrase.substring(divider+1, string_length);
			Translator translator_1 = new Translator(first_phrase);
			String first_piece = translator_1.translate();
			Translator translator_2 = new Translator(second_phrase);
			String second_piece = translator_2.translate();
			return first_piece + " " + second_piece;	
		}
		return null;
	}
	
	
	private boolean findPunctuationMarks() {	
		return (phrase.indexOf(".") != -1 || phrase.indexOf(",") != -1 || phrase.indexOf(";") != -1 || phrase.indexOf(":") != -1 || phrase.indexOf("?") != -1 || phrase.indexOf("!") != -1 || phrase.indexOf("'") != -1 || phrase.indexOf("(") != -1 || phrase.indexOf(")")!= -1);		
	}
	
	private String translatePhraseWithMoreWordsAndPunctuationMarks() {
		if(findPunctuationMarks()) {
			int length = phrase.length();
			char mark = phrase.charAt(length-1);
			String newString = phrase.substring(0, length-1);
			Translator translator_1 = new Translator(newString);
			return translator_1.translate() + mark;
		}
		return "";
	}

}
